[![](https://i.creativecommons.org/l/by-sa/4.0/88x31.png)](./LICENSE.md)

Projeto de pesquisa submetido a diversas chamadas públicas de processos seletivos para estágio pós-doutoral.

Nesse projeto, pretendemos investigar algumas questões de interesse da área de Geometria Diferencial, à saber, *desigualdades geométricas para buracos negros e outros corpos*, questões estas que advêm do cenário de Relatividade Geral.